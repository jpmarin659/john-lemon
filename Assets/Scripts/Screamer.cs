using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screamer : MonoBehaviour
{
    public GameObject imegenScreamer;
    public GameObject sonidoScreamer;
    //activar
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            imegenScreamer.SetActive(true);
            imegenScreamer.SetActive(true);
        }
    }
    //desactivar
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            imegenScreamer.SetActive(false);
            imegenScreamer.SetActive(false);

            // se destruye
            Destroy(this);
        }
    }
}
